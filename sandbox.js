var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    projectUtility = require('annulet-models')
    .util,
    config = require('annulet-config'),
    request = require('request'),
    ix = require('./index');
_.mixin(require('annulet-util')
    .lodashMixins);


var logger = require('winston');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});
logger.debug('seeding');

async.waterfall([

        function(cb) {
            modeler.setup(config.configuration.paths.dbListing(), function(err) {
                logger.silly(util.inspect(err));
                cb(err);
            });
        },
        function(cb) {
            logger.silly('getting models');
            modeler.db({
                collection: 'Customer',
                query: {
                    name: 'Test Company that has an excessively long, *really* long name',
                    active: true,
                    deleted: false
                }
            }, function(err, models) {
                //if (!!models) {
                //    cb('customer existed, skipping');
                //} else {
                cb(err, models, {});
                //}
            });
        },
        function(models, p, cb) {
            logger.silly('test customer did not exist, retrieving default and creating');
            projectUtility.customer.create({
                customer: {
                    name: 'Test Company that has an excessively long, *really* long name',
                    address: {
                        street1: 'one',
                        city: 'city',
                        state: 'state',
                        zip: 'zip'
                    },
                    phone: 'phone',
                    numberOfLocations: 1,
                    numberOfEmployees: 1,
                    stripeCustomerId: 'cus_5lIaShPYbBlMRk' //hack: for now, use an already-made customer ID
                }
            }, function(err, models, customer) {
                if (!!err) {
                    logger.error('problem making customer: ' + util.inspect(err));
                }

                logger.silly('customer made');
                p.customer = customer;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('saving sudoer');
            var url = config.configuration.paths.apiUri() + '/admin/userManagement/save';
            logger.silly('[sandbox] url: ' + url);

            request.post({
                json: true,
                url: url,
                headers: {
                    'annulet-internal': config.configuration.paths.internalKey(),
                    'annulet-auth-customer': p.customer._id
                },
                body: {
                    user: config.configuration.administrativeUser(),
                    customer: p.customer._id,
                    roles: ['Owner', 'Administrator', 'User', 'System User']
                }
            }, function(err, response, body) {
                p.user = body.data;
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            logger.silly('about to prop up customer: ' + p.customer._id);
            ix.customer({
                customer: {
                    _id: p.customer._id
                }
            }, function(err, stuff) {
                cb(err, models, p);
            });
        },
        function(models, p, cb) {
            models.definitions.workflow.workflowItemTypes.Root.find({
                active:true,
                deleted:false
            })
                .exec(function(err, roots) {
                    logger.silly('roots found: ' + util.inspect(roots));
                    p.roots = roots;
                    cb(err, models, p);
                });
        },
        function(models, p, cb) {
            logger.silly((new Array(96))
                .join('='));
            projectUtility.definition.workflow.loader(models, {
                workflowItemDefinitions: p.roots,
                flatten:false
            }, function(err, models, loaded) {
                logger.silly(JSON.stringify(loaded, null, 3));
                cb(err, models, p);
            });
        }
    ],
    function(err, r) {
        if (!!err) {
            logger.warn('seed err: ' + util.inspect(err));
        }
        logger.info('seed complete');
    });
    
