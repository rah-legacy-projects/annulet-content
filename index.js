var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    path = require('path'),
    imports = require('./import'),
    push = require('./push');

_.mixin(require('annulet-util')
    .lodashMixins);
var ix = {};
ix.customer = function(options, cb) {
    var customerId = _.extractId(options, 'customer');
    async.waterfall([

        function(cb) {
            cb(null, {});
        },
        function(p, cb) {
            //log in if not logged in
            if (!options.authToken) {
                logger.silly('[customer content] logging in ...');
                push.login(null, function(err, authToken) {
                    logger.silly('[customer content] token: '  + authToken);
                    options.authToken = authToken;
                    cb(err, p);
                });
            } else {
                cb(null, p);
            }
        },
        function(p, cb) {
            logger.silly('[customer content] about to commit content');
            async.series({
                workflows: function(cb) {

                    async.waterfall([

                        function(cb) {
                            cb(null, {})
                        },
                        function(p, cb) {
                            logger.silly('getting workflow directories');
                            //get the section directories
                            fs.readdir(path.resolve(__dirname, './content/workflow'), function(err, files) {
                                async.series(_.map(files, function(file) {
                                    return function(cb) {
                                        var fpath = path.resolve(__dirname, './content/workflow', file);
                                        fs.stat(fpath, function(err, stat) {
                                            if (stat.isDirectory()) {
                                                cb(null, fpath);
                                            } else {
                                                cb(null);
                                            }
                                        });
                                    };
                                }), function(err, r) {
                                    p = {};
                                    p.directories = _.filter(r, function(s) {
                                        return !!s;
                                    });
                                    cb(err, p);
                                });
                            });
                        },
                        function(p, cb) {
                            logger.silly('[customer content] pulling content up');
                            var problems = [];
                            async.series(_.map(p.directories, function(directory) {
                                return function(cb) {
                                    imports.workflow({
                                        path: directory
                                    }, function(err, wf, opProblems) {
                                        problems = problems.concat(opProblems);
                                        cb(err, wf);
                                    });
                                };
                            }), function(err, r) {
                                p.loadedWorkflows = r;
                                p.problems = problems;
                                cb(err, p);
                            });
                        },
                        function(p, cb) {
                            if (!!options.dryRun) {
                                logger.info('Dry run, skipping saving.');
                                cb(null, p);
                            } else {
                                logger.silly('saving');
                                async.series(_.map(p.loadedWorkflows, function(wf) {
                                        return function(cb) {
                                            logger.silly('saving wf ' + wf.shortName);
                                            async.series({
                                                training: function(cb) {
                                                    var wfis = _.filterDeep(wf, {
                                                        __t: function(key, value) {
                                                            return /workflowItemTypes\.Training$/.test(value);
                                                        }
                                                    });
                                                    wfis = _.compact(wfis);
                                                    logger.silly('training items: ' + wfis.length);
                                                    async.series(_.map(wfis, function(wfi) {
                                                        return function(cb) {
                                                            logger.silly('pushing training ' + wfi.training.shortName);
                                                            push.training({
                                                                customer: customerId,
                                                                authToken: options.authToken,
                                                                training: wfi.training
                                                            }, function(err, training) {
                                                                logger.silly('[ixpush] training: ' + util.inspect(training));
                                                                wfi.training = training._id;
                                                                cb(err, wfi);
                                                            });
                                                        };
                                                    }), function(err, r) {
                                                        cb(err, r);
                                                    });
                                                },
                                                quiz: function(cb) {
                                                    var wfis = _.filterDeep(wf, {
                                                        __t: function(key, value) {
                                                            return /workflowItemTypes\.Quiz$/.test(value);
                                                        }
                                                    });
                                                    wfis = _.compact(wfis);
                                                    logger.silly('quiz items: ' + wfis.length);
                                                    async.series(_.map(wfis, function(wfi) {
                                                        return function(cb) {
                                                            logger.silly('pushing quiz ' + wfi.quiz.shortName);
                                                            push.quiz({
                                                                customer: customerId,
                                                                authToken: options.authToken,
                                                                quiz: wfi.quiz
                                                            }, function(err, quiz) {
                                                                wfi.quiz = quiz._id;
                                                                cb(err, wfi);
                                                            });
                                                        };
                                                    }), function(err, r) {
                                                        //saev the workflow itself
                                                        push.workflowItem({
                                                            customer: customerId,
                                                            authToken: options.authToken,
                                                            workflow: wf
                                                        }, function(err, wf) {
                                                            p.workflowDefinition = wf;
                                                            cb(err, r);
                                                        })
                                                    });
                                                }
                                            }, function(err, r) {
                                                cb(err, r.workflowDefinition);
                                            });
                                        }
                                    }),
                                    function(err, p) {
                                        cb(err, p);
                                    });
                            }
                        }
                    ], function(err, r) {
                        p.workflows = r;
                        cb(err, p);
                    })
                },
                operatingProcedures: function(cb) {
                    async.waterfall([

                        function(cb) {
                            logger.silly('getting op directories');
                            //get the section directories
                            fs.readdir(path.resolve(__dirname, './content/operatingProcedures'), function(err, files) {
                                async.series(_.map(files, function(file) {
                                    return function(cb) {
                                        var fpath = path.resolve(__dirname, './content/operatingProcedures', file);
                                        fs.stat(fpath, function(err, stat) {
                                            if (stat.isDirectory()) {
                                                cb(null, fpath);
                                            } else {
                                                cb(null);
                                            }
                                        });
                                    };
                                }), function(err, r) {
                                    p = {};
                                    p.directories = _.filter(r, function(s) {
                                        return !!s;
                                    });
                                    cb(err, p);
                                });
                            });
                        },
                        function(p, cb) {
                            var problems = [];
                            async.series(_.map(p.directories, function(directory) {
                                return function(cb) {
                                    imports.operatingProcedure({
                                        path: directory
                                    }, function(err, op, opProblems) {
                                        problems = problems.concat(opProblems);
                                        cb(err, op);
                                    });
                                };
                            }), function(err, r) {
                                p.loadedOPs = r;
                                p.problems = problems;
                                cb(err, p);
                            });
                        },
                        function(p, cb) {
                            if (!!options.dryRun) {
                                logger.info('Dry run, skipping saving.');
                                cb(null, p);
                            } else {
                                async.series(_.map(p.loadedOPs, function(op) {
                                    return function(cb) {
                                        logger.silly('saving ' + op.title);
                                        if (!op) {
                                            logger.warn('OP had file problems, skipping.');
                                        } else if (op.hasProblems) {
                                            logger.info('op ' + op.title + ' has problems, skipping.');
                                            cb(null);
                                        } else {
                                            op.sections = _.compact(op.sections);
                                            op.sections = _.filter(op.sections, function(s) {
                                                return !s.hasProblems;
                                            });
                                            push.operatingProcedure({
                                                customer: customerId,
                                                authToken: options.authToken,
                                                operatingProcedure: op
                                            }, function(err, operatingProcedure) {
                                                cb(err, operatingProcedure);
                                            });
                                        }
                                    };
                                }), function(err, r) {
                                    p.operatingProcedures = r;
                                    cb(err, p);
                                });
                            }
                        }
                    ], function(err, r) {
                        p.operatingProcedures = r;
                        cb(err, p);
                    });
                }
            }, function(err, r) {
                cb(err, {
                    customer: customerId,
                    operatingProcedures: r.operatingProcedures.operatingProcedures,
                    workflows: r.workflows.workflows,
                    operatingProcedureProblems: r.operatingProcedures.problems,
                    workflowProblems: r.workflows.problems
                });
            });
        }
    ], function(err, p) {
        cb(err, p);
    });
};

ix.allCustomers = function(options, cb){
    async.waterfall([
        function(cb){
            cb(null, {});
        },
        function(p, cb){
            push.listCustomers({
                authToken: options.authToken
            }, function(err, customers){
                p.customers = customers;
                cb(err, p);
            });
        },
        function(p, cb){
            async.series(_.map(p.customers, function(customer){
                return function(cb){
                    ix.customer({
                        customer: customer,
                        authToken: options.authToken
                    }, function(err, loadResults){
                        cb(err, loadRseults);
                    });
                }
            }), function(err, r){
                p.customerResults = r;
                cb(err, p);
            });
        }
    ], function(err, p){
        cb(err, p);
    });
};

module.exports = exports = ix;
