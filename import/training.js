var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    util = require('util'),
    moment = require('moment'),
    line = require('./line'),
    fs = require('fs');

module.exports = function(options, cb) {

    var problems = [];

    async.waterfall([

            function(cb) {
                cb(null, {});
            },
            function(p, cb) {
                if (!fs.existsSync(path.resolve(options.path))) {
                    return cb('path ' + options.path + ' did not exist.');
                }
                return cb(null, p);
            },
            function(p, cb) {
                //if the name exists, open it
                var namepath = path.resolve(options.path, 'name.md');
                if (!fs.existsSync(namepath)) {
                    cb('name missing for op at ' + namepath);
                } else {
                    cb(null, {
                        training: {},
                        namepath: namepath
                    });
                }

            },
            function(p, cb) {
                logger.silly('getting name content for the training');
                //get the name content
                var namecontent = fs.readFile(p.namepath, function(err, data) {
                    p.namecontent = data.toString();
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling out title and desc');
                //pull the title and desc out of name
                p.training.title = line(p.namecontent, /^\s*#(.+)$/);
                p.training.description = line(p.namecontent, /^\s*([^#].+)$/);
                p.training.shortName = line(p.namecontent, /^shortname\:\s*(.+)/);

                if (/^\s*$/.test((p.training.title || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Title is blank.'
                    });
                }
                if (/^\s*$/.test((p.training.description || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Description is blank.'
                    });
                }
                if (/^\s*$./.test((p.training.shortName || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Training did not have short name.'
                    });
                }
                cb(null, p);
            },
            function(p, cb) {
                logger.silly('getting section directory');
                //get the section directories
                fs.readdir(path.resolve(options.path, 'sections'), function(err, files) {
                    async.parallel(_.map(files, function(file) {
                        return function(cb) {
                            var fpath = path.resolve(options.path, 'sections', file);
                            cb(null, fpath);
                        };
                    }), function(err, r) {
                        p.sectionfiles = _.filter(r, function(s) {
                            return !!s;
                        });
                        cb(err, p);
                    });
                });
            },
            function(p, cb) {
                logger.silly('getting sections');
                async.series(_.map(p.sectionfiles, function(sectionfile) {
                    return function(cb) {
                        fs.readFile(sectionfile, function(err, content) {
                            content = content.toString();
                            lines = content.split('\n');
                            var section = {};
                            logger.silly('headline: ' + lines[0]);
                            section.title = /^\s*#(.+)$/.exec(lines[0])[1];
                            section.markdown = _.chain(lines.slice(1))
                                .dropWhile(function(line) {
                                    return /^\s*$/.test(line);
                                })
                                .dropRightWhile(function(line) {
                                    return /^\s*$/.test(line);
                                })
                                .value()
                                .join('\n');
                            section.__t = 'definitions.training.sectionTypes.MarkdownContent';
                            section._rangeType = 'definitions.training.ranged.sectionTypes.MarkdownContent';

                            if (/^\s*$/.test((section.title || ''))) {
                                problems.push({
                                    file: sectionfile,
                                    problem: 'Title is blank.'
                                });
                            }
                            if (/^\s*$/.test((section.markdown || ''))) {
                                problems.push({
                                    file: sectionfile,
                                    problem: 'Content is blank.'
                                });
                            }

                            cb(null, section);
                        });
                    };
                }), function(err, r) {
                    p.training.sections = r;
                    cb(err, p);
                });
            },
            function(p, cb){
                if(!p.training.startDate){
                    p.training.startDate = moment().toDate();
                }

                if(!p.training.endDate){
                    p.training.endDate = 'forever';
                }

                cb(null, p);
            }
        ],
        function(err, p) {
            if (!!err) {
                logger.error('[training] ' + util.inspect(err));
            }
            p.training.$d = 'Training';
            logger.silly('operating procedure complete');
            cb(err, p.training, problems);
        });
};
