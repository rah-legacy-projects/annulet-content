var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    line = require('./line'),
    moment = require('moment'),
    util = require('util'),
    sections = require('./operatingProcedureSection'),
    fs = require('fs');

module.exports = function(options, cb) {

    var problems = [];
    async.waterfall([

            function(cb) {
                //if the name exists, open it
                var namepath = path.resolve(options.path, 'name.md');
                if (!fs.existsSync(namepath)) {
                    cb('name missing for op at ' + namepath);
                } else {
                    cb(null, {
                        operatingProcedure: {},
                        namepath: namepath
                    });
                }

            },
            function(p, cb) {
                logger.silly('getting name content for the op');
                //get the name content
                var namecontent = fs.readFile(p.namepath, function(err, data) {
                    p.namecontent = data.toString();
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling out title and desc');
                //pull the title and desc out of name
                p.operatingProcedure.title = line(p.namecontent, /^\s*#(.+)$/);
                p.operatingProcedure.description = line(p.namecontent.replace(/shortname\:\s*.+/, ''), /^\s*([^#].+)$/);
                p.operatingProcedure.shortName = line(p.namecontent, /^shortname\:\s*(.+)/);
                if (/^\s*$/.test((p.operatingProcedure.title || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Title is blank.'
                    });
                }
                if (/^\s*$/.test((p.operatingProcedure.description || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Description is blank.'
                    });
                }
                if (/^\s*$./.test((p.operatingProcedure.shortName || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'OperatingProcedure did not have short name.'
                    });
                }
                cb(null, p);
            },
            function(p, cb) {
                logger.silly('getting section directories');
                //get the section directories
                fs.readdir(options.path, function(err, files) {
                    async.series(_.map(files, function(file) {
                        return function(cb) {
                            var fpath = path.resolve(options.path, file);
                            fs.stat(fpath, function(err, stat) {
                                if (stat.isDirectory()) {
                                    cb(null, fpath);
                                } else {
                                    cb(null);
                                }
                            });
                        };
                    }), function(err, r) {
                        p.directories = _.filter(r, function(s) {
                            return !!s;
                        });
                        cb(err, p);
                    });
                });
            },
            function(p, cb) {
                logger.silly('procesing section directories');
                //determine the directory type and process appropriately
                async.series(_.map(p.directories, function(directory) {
                    return function(cb) {
                        logger.silly('processing ' + directory);
                        fs.readdir(directory, function(err, files) {
                            if (_.any(files, function(file) {
                                return /terms/i.test(file);
                            })) {
                                //definitions
                                sections.definition({
                                    path: directory
                                }, function(err, section, sectionProblems) {
                                    logger.silly('[op] def processed.');
                                    problems = problems.concat(sectionProblems);
                                    cb(err, section);
                                });
                            } else if (_.any(files, function(file) {
                                return /subsections/i.test(file);
                            })) {
                                //sectioned content
                                sections.sectionedContent({
                                    path: directory
                                }, function(err, section, sectionProblems) {
                                    logger.silly('[op] sectioned content processed.');
                                    problems = problems.concat(sectionProblems);
                                    cb(err, section);
                                });
                            } else {
                                //content
                                sections.content({
                                    path: directory
                                }, function(err, section, sectionProblems) {
                                    logger.silly('[op] def content processed');
                                    problems = problems.concat(sectionProblems);
                                    cb(err, section);
                                });
                            }
                        });
                    };
                }), function(err, r) {
                    p.operatingProcedure.sections = r;
                    cb(err, p);
                });
            },
            function(p, cb){
                if(!p.operatingProcedure.startDate){
                    p.operatingProcedure.startDate = moment().toDate();
                }

                if(!p.operatingProcedure.endDate){
                    p.operatingProcedure.endDate = 'forever';
                }

                cb(null, p);
            }
        ],
        function(err, p) {
            logger.silly('operating procedure complete');
            if (!p) {
                cb(err, null, problems);
            } else {
                cb(err, p.operatingProcedure, problems);
            }
        });
};
