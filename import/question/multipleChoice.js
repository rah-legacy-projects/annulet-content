var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    util = require('util'),
    line = require('../line');

module.exports = function(options, cb) {
    var problems = [];
    async.waterfall([

        function(cb) {
            //get the definition files
            fs.readdir(path.resolve(options.path), function(err, files) {
                async.parallel(_.map(files, function(file) {
                    return function(cb) {
                        var fpath = path.resolve(options.path, file);
                        logger.silly('[multichoice] fpath: ' + fpath);
                        fs.stat(fpath, function(err, stat) {
                            if (!!err) {
                                logger.error(err);
                            }
                            if (!stat.isDirectory()) {
                                cb(null, fpath);
                            } else {
                                cb(null);
                            }
                        });
                    };
                }), function(err, r) {
                    var p = {};
                    p.questionfiles = _.filter(r, function(s) {
                        return !!s;
                    });
                    cb(err, p);
                });
            });
        },
        function(p, cb) {
            logger.silly('[multichoice] getting the questions out of ' + p.questionfiles.length + ' files')
            //get the questions out of the files
            async.series(_.map(p.questionfiles, function(questionfile) {
                return function(cb) {
                    fs.readFile(questionfile, function(err, content) {
                        content = content.toString();
                        var question = {
                            __t: 'definitions.quiz.questionTypes.MultipleChoice',
                            _rangeType: 'definitions.quiz.ranged.questionTypes.MultipleChoice',
                            required: false,
                        };
                        question.questionText = line(content, /^\s*#(.+)$/);
                        question.answers = _.chain(content.split('\n'))
                            .filter(function(s) {
                                return ((/^\d+\..+$/.test(s)));
                            })
                            .map(function(s) {
                                var output = /\d+\.\s*(\{correct\})?\s*(.+)$/.exec(s);

                                if (/^\s*$/.test((output[2] || ''))) {
                                    problems.push({
                                        file: questionfile,
                                        problem: 'No answer text for question'
                                    });
                                }

                                return {
                                    answerText: output[2],
                                    isCorrect: !!output[1]
                                };
                            })
                            .value();
                        question.answerQuota = question.answers.length;
                        if (/^\s*$/.test((question.questionText || ''))) {
                            problems.push({
                                file: questionfile,
                                problem: 'Question had no question text.'
                            });
                        }
                        if (question.answers.length == 0) {
                            problems.push({
                                file: questionfile,
                                problem: "No answers provided."
                            });
                        }
                        if (!_.any(question.answers, function(a) {
                            return a.isCorrect;
                        })) {
                            problems.push({
                                file: questionfile,
                                problem: "No correct answers found."
                            });
                        }
                        cb(err, question);
                    });
                };
            }), function(err, r) {
                p.questions = r;
                cb(err, p);
            });
        }
    ], function(err, p) {
        //give the definition back
        logger.debug('[multichoice def] definition retrieved.');
        cb(err, p.questions, problems);
    });
};
