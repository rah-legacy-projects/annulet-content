var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    util = require('util'),
    line = require('../line');

module.exports = function(options, cb) {
    var problems = [];
    async.waterfall([

            function(cb) {
                //get the definition files
                fs.readdir(path.resolve(options.path), function(err, files) {
                    async.parallel(_.map(files, function(file) {
                        return function(cb) {
                            var fpath = path.resolve(options.path, file);
                            logger.silly('[truefalse] fpath: ' + fpath);
                            fs.stat(fpath, function(err, stat) {
                                if (!!err) {
                                    logger.error(err);
                                }
                                if (!stat.isDirectory()) {
                                    cb(null, fpath);
                                } else {
                                    cb(null);
                                }
                            });
                        };
                    }), function(err, r) {
                        var p = {};
                        p.questionfiles = _.filter(r, function(s) {
                            return !!s;
                        });
                        cb(err, p);
                    });
                });
            },
            function(p, cb) {
                logger.silly('[truefalse] getting the questions out of ' + p.questionfiles.length + ' files')
                //get the questions out of the files
                async.series(_.map(p.questionfiles, function(questionfile) {
                        return function(cb) {
                            fs.readFile(questionfile, function(err, content) {
                                content = content.toString();
                                var question = {
                                    __t: 'definitions.quiz.questionTypes.TrueFalse',
                                    _rangeType: 'definitions.quiz.ranged.questionTypes.TrueFalse',
                                        required: false,
                                };
                                question.questionText = line(content, /^\s*#(.+)$/);

                                var answer = _.chain(content.split('\n'))
                                    .filter(function(s) {
                                        return ((/^\d+\..+$/.test(s)));
                                    })
                                    .map(function(s) {
                                        var output = /\d+\.\s*(\{correct\})?\s*(.+)$/.exec(s);
                                        logger.silly(output[1] + ' ' + output[2]);
                                        if (/^\s*$/.test((output[2] || ''))) {
                                            problems.push({
                                                file: questionfile,
                                                problem: 'No answer text for question'
                                            });
                                        }

                                        return {
                                            file: questionfile,
                                            answerText: output[2],
                                            isCorrect: !!output[1]
                                        };
                                    })
                                    .find(function(s) {
                                        logger.silly(s.isCorrect + ' ' + s.answerText)
                                        return s.isCorrect;
                                    })
                                    .value();

                                question.correctAnswer = (/True/i)
                                    .test((answer || {})
                                        .answerText);

                                cb(err, question);
                            });
                        };
                    }),
                    function(err, r) {
                        p.questions = r;
                        cb(err, p);
                    });
            }
        ],
        function(err, p) {
            //give the definition back
            logger.debug('[truefalse def] definition retrieved.');
            cb(err, p.questions, problems);
        });
};
