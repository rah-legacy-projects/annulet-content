module.exports = exports = {
	multipleChoice: require("./multipleChoice"),
	trueFalse: require("./trueFalse"),
	oneChoice: require('./oneChoice')
};
