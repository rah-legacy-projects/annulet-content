var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    line = require('./line'),
    fs = require('fs'),
    quizImport = require('./quiz'),
    util = require('util'),
    moment = require('moment'),
    trainingImport = require('./training');
module.exports = function(options, cb) {

    var problems = [];

    async.waterfall([

            function(cb) {
                logger.silly('workflow content path: ' + options.path);
                //if the name exists, open it
                var namepath = path.resolve(options.path, 'name.md');
                if (!fs.existsSync(namepath)) {
                    cb('name missing for workflow at ' + namepath);
                } else {
                    cb(null, {
                        rootItem: {},
                        namepath: namepath
                    });
                }

            },
            function(p, cb) {
                logger.silly('getting name content for the workflow');
                //get the name content
                var namecontent = fs.readFile(p.namepath, function(err, data) {
                    p.namecontent = data.toString();
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling out title and desc');
                //pull the title and desc out of name
                p.rootItem.title = line(p.namecontent, /^\s*#(.+)$/);
                p.rootItem.description = line(p.namecontent.replace(/shortname\:\s*.+/, ''), /^\s*([^#].+)$/);
                p.rootItem.shortName = line(p.namecontent, /^shortname\:\s*(.+)/);

                if (/^\s*$/.test((p.rootItem.name || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Root item name is blank.'
                    });
                }
                if (/^\s*$/.test((p.rootItem.description || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Root item description is blank.'
                    });
                }
                if (/^\s*$./.test((p.rootItem.shortName || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'root item did not have short name.'
                    });
                }

                p.rootItem.__t = 'definitions.workflow.workflowItemTypes.Root';
                p.rootItem._rangeType= 'definitions.workflow.ranged.workflowItemTypes.Root';
                p.rootItem.items = [{
                    title: p.rootItem.title,
                    __t: 'definitions.workflow.workflowItemTypes.Container',
                    _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Container',
                    sequence: []
                }];
                p.rootItem.employeeClass = ['Federal'];
                cb(null, p);
            },
            function(p, cb) {
                async.parallel({

                    training: function(cb) {
                        trainingImport({
                            path: path.resolve(options.path, 'training')
                        }, function(err, training, trainingProblems) {
                            if (!!err) {
                                problems.push({
                                    file: null,
                                    problem: err
                                });
                            }
                            problems = problems.concat(trainingProblems || []);
                            cb(null, training);
                        });
                    },
                    quiz: function(cb) {
                        quizImport({
                            path: path.resolve(options.path, 'quiz')
                        }, function(err, quiz, quizProblems) {
                            if (!!err) {
                                problems.push({
                                    file: null,
                                    problem: err
                                });
                            }
                            problems = problems.concat(quizProblems || []);
                            cb(null, quiz);
                        });
                    }
                }, function(err, r) {
                    if (!!r.training) {
                        p.rootItem.items[0].sequence.push({
                            title: r.training.title,
                            description: r.training.description,
                            __t: 'definitions.workflow.workflowItemTypes.Training',
                            _rangeType: 'definitions.workflow.ranged.workflowItemTypes.Training',
                            training: r.training
                        });
                    }
                    if (!!r.quiz) {
                        p.rootItem.items[0].sequence.push({
                            title: r.quiz.title,
                            description: r.quiz.description,
                            __t: 'definitions.workflow.workflowItemTypes.Quiz',
                            _rangeType:'definitions.workflow.ranged.workflowItemTypes.Quiz',
                            quiz: r.quiz
                        });
                    }
                    cb(err, p);
                });
            },
            function(p, cb){
                if(!p.rootItem.startDate){
                    p.rootItem.startDate = moment().toDate();
                }

                if(!p.rootItem.endDate){
                    p.rootItem.endDate = 'forever';
                }

                cb(null, p);
            }
        ],
        function(err, p) {
            if (!!err) {
                logger.error('[worfklow content] ' + util.inspect(err));
            }
            logger.silly('workflow complete');

            cb(err, p.rootItem, problems);
        });
};
