var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    util = require('util'),
    mkdirp = require('mkdirp'),
    line = require('../line');

module.exports = function(options, cb) {
    var problems = [];
    async.waterfall([

        function(cb) {
            //get the definition files
            fs.readdir(path.resolve(options.path), function(err, files) {
                async.parallel(_.map(files, function(file) {
                    return function(cb) {
                        var fpath = path.resolve(options.path, file);
                        logger.silly('[multichoice] fpath: ' + fpath);
                        fs.stat(fpath, function(err, stat) {
                            if (!!err) {
                                logger.error(err);
                            }
                            if (!stat.isDirectory()) {
                                cb(null, fpath);
                            } else {
                                cb(null);
                            }
                        });
                    };
                }), function(err, r) {
                    var p = {};
                    p.questionfiles = _.filter(r, function(s) {
                        return !!s;
                    });
                    cb(err, p);
                });
            });
        },
        function(p, cb) {
            //get the questions out of the files
            async.series(_.map(p.questionfiles, function(questionfile) {
                return function(cb) {
                    fs.readFile(questionfile, function(err, content) {
                        content = content.toString();
                        var count = _.chain(content.split('\n'))
                            .filter(function(s) {
                                return ((/^\d+\..+$/.test(s)));
                            })
                            .map(function(s) {
                                var output = /\d+\.\s*(\{correct\})?\s*(.+)$/.exec(s);
                                if(!!output[1]){
                                    return output[2];
                                }
                                return null;
                            })
                            .compact()
                            .value()
                            .length;
                        
                        if(count > 1){
                            //move the file to the single choice dir
                            var newLocation = questionfile.replace('oneChoice', 'multipleChoice');
                            var newLocationDirectory = path.dirname(newLocation);
                            mkdirp.sync(newLocationDirectory);
                            fs.renameSync(questionfile, newLocation);
                        }
                        //let single answers and no answers fall through
                        
                        cb(err);
                    });
                };
            }), function(err, r) {
                cb(err);
            });
        }
    ], function(err) {
        cb(err);
    });
};
