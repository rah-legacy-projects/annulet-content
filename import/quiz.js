var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    line = require('./line'),
    util = require('util'),
    questions = require('./question'),
    mv = require('./mv'),
    moment = require('moment'),
    fs = require('fs');

module.exports = function(options, cb) {
    var problems = [];
    async.waterfall([

            function(cb) {
                cb(null, {});
            },
            function(p, cb) {
                if (!fs.existsSync(path.resolve(options.path))) {
                    return cb('quiz path ' + options.path + ' did not exist.');
                }
                return cb(null, p);
            },
            function(p, cb) {
                //if the name exists, open it
                var namepath = path.resolve(options.path, 'name.md');
                if (!fs.existsSync(namepath)) {
                    cb('name missing for op at ' + namepath);
                } else {
                    cb(null, {
                        quiz: {},
                        namepath: namepath
                    });
                }

            },
            function(p, cb) {
                logger.silly('getting name content for the op');
                //get the name content
                var namecontent = fs.readFile(p.namepath, function(err, data) {
                    p.namecontent = data.toString();
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('pulling out title and desc');
                //pull the title and desc out of name
                p.quiz.title = line(p.namecontent, /^\s*#(.+)$/);
                p.quiz.directions = line(p.namecontent, /^\s*([^#].+)$/);
                p.quiz.passingPercentage = parseInt(line(p.namecontent, /^passing percentage\:\s*(\d+)\%/), 10);
                p.quiz.questionQuota = parseInt(line(p.namecontent, /^question quota\:\s*(\d+)/), 10);
                p.quiz.shortName = line(p.namecontent, /^shortname\:\s*(.+)/);

                if (/^\s*$/.test((p.quiz.title || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Title is blank.'
                    });
                }
                if (/^\s*$/.test((p.quiz.directions || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Directions is blank.'
                    });
                }
                if (/^\s*$/.test((p.quiz.passingPercentage || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Passing percentage did not exist, using default of 70.'
                    });
                }

                if (/^\s*$/.test((p.quiz.questionQuota || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Question quota did not exist, using default of 5.'
                    });
                }
                if (/^\s*$./.test((p.quiz.shortName || ''))) {
                    problems.push({
                        file: p.namepath,
                        problem: 'Quiz did not have short name.'
                    });
                }

                p.quiz.passingPercentage = p.quiz.passingPercentage || 70;
                p.quiz.questionQuota = p.quiz.questionQuota || 5;


                cb(null, p);
            },

            function(p, cb) {
                logger.silly('getting question directories');
                //get the qusestion directories
                fs.readdir(options.path, function(err, files) {
                    async.series(_.map(files, function(file) {
                        return function(cb) {
                            var fpath = path.resolve(options.path, file);
                            fs.stat(fpath, function(err, stat) {
                                if (stat.isDirectory()) {
                                    cb(null, fpath);
                                } else {
                                    cb(null);
                                }
                            });
                        };
                    }), function(err, r) {
                        p.directories = _.filter(r, function(s) {
                            return !!s;
                        });
                        cb(err, p);
                    });
                });
            },
            function(p, cb) {
                //hack: arrange single-choice questions in single-choice directories and multi-choice in multi-choice directories
                logger.silly('arranging multi and single-choice directories');
                async.series(_.map(p.directories, function(directory) {
                    return function(cb) {
                        logger.silly('processing ' + directory);
                        if (/multipleChoice/i.test(directory)) {
                            mv.multipleChoice({
                                path: directory
                            }, function(err) {
                                logger.silly('[mv] multiple choice processed.');
                                cb(err, questions);
                            });
                        } else if (/oneChoice/i.test(directory)) {
                            mv.oneChoice({
                                path: directory
                            }, function(err, questions, qproblems) {
                                logger.silly('[mv] single choice procesed.');
                                cb(err);
                            });
                        } else {
                            cb(null);
                        }
                    }
                }), function(err, r) {
                    cb(err, p);
                });
            },
            function(p, cb) {
                logger.silly('procesing question directories');
                //determine the directory type and process appropriately
                async.series(_.map(p.directories, function(directory) {
                    return function(cb) {
                        logger.silly('processing ' + directory);
                        if (/multipleChoice/i.test(directory)) {
                            questions.multipleChoice({
                                path: directory
                            }, function(err, questions, qproblems) {
                                problems = problems.concat(qproblems);
                                logger.silly('[quiz] multiple choice processed.');
                                cb(err, questions);
                            });
                        } else if (/oneChoice/i.test(directory)) {
                            questions.oneChoice({
                                path: directory
                            }, function(err, questions, qproblems) {
                                problems = problems.concat(qproblems);
                                logger.silly('[quiz] single choice procesed.');
                                cb(err, questions);
                            });
                        } else if (/trueFalse/i.test(directory)) {
                            questions.trueFalse({
                                path: directory
                            }, function(err, questions, qproblems) {
                                problems = problems.concat(qproblems);
                                logger.silly('[quiz] true/false processed.');
                                cb(err, questions);
                            });
                        } else {
                            problems.push({
                                file: directory,
                                problem: 'Could not figure out how to process.'
                            });
                            logger.warn('[quiz] did not know how to handle question directory ' + directory);
                            cb(null);
                        }
                    };
                }), function(err, r) {
                    r = _.reduce(r, function(a, b) {
                        return a.concat(b);
                    }, []);
                    p.quiz.questions = r;
                    cb(err, p);
                });
            },
            function(p, cb){
                if(!p.quiz.startDate){
                    p.quiz.startDate = moment().toDate();
                }

                if(!p.quiz.endDate){
                    p.quiz.endDate = 'forever';
                }

                cb(null, p);
            }

        ],
        function(err, p) {
            if (!!err) {
                logger.error(util.inspect(err));
                return cb(err, p);
            }
            p.quiz.$d = 'Quiz';
            logger.silly('quiz complete');
            cb(err, p.quiz, problems);
        });
};
