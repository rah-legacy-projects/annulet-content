var _ = require('lodash');
module.exports = exports = function(content, regex) {
    return _.chain(content.split('\n'))
        .map(function(s) {
            return ((regex.exec(s)) || [])[1];
        })
        .filter(function(s) {
            return !!s;
        })
        .value()
        .join('\n');
};
