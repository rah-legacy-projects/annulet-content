module.exports = exports = {
	content: require("./content"),
	definition: require("./definition"),
	sectionedContent: require("./sectionedContent"),
};
