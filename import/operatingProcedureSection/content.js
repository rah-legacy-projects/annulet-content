var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    util = require('util'),
    line = require('../line');

module.exports = function(options, cb) {
    logger.silly('[op content] path: ' + options.path);
    var problems = [];
    async.waterfall([

        function(cb) {
            logger.silly('[op content] getting file');
            //if the name exists, open it
            var namepath = path.resolve(options.path, 'content.md');
            logger.silly('[op content] path: ' + namepath);
            if (!fs.existsSync(namepath)) {
                namepath = path.resolve(options.path, 'name.md');
                if (!fs.existsSync(namepath)) {
                    cb('content missing for op at ' + namepath);
                }
            } else {
                cb(null, {
                    namepath: namepath,
                    section: {
                        __t: 'definitions.operatingProcedure.sectionTypes.Content',
                        _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Content'
                    }
                });
            }

        },
        function(p, cb) {
            logger.silly('[op content] getting file content');
            //get the name content
            var content = fs.readFile(p.namepath, function(err, data) {
                p.content = data.toString();
                cb(err, p);
            });
        },
        function(p, cb) {
            logger.silly('[op content] pulling content');
            //pull the title and desc out of content
            lines = p.content.split('\n');
            p.section.title = /^\s*#(.+)$/.exec(lines[0])[1];
            p.section.content = _.chain(lines.slice(1))
                .dropWhile(function(line) {
                    return /^\s*$/.test(line);
                })
                .dropRightWhile(function(line) {
                    return /^\s*$/.test(line);
                })
                .value()
                .join('\n');

            if (/^\s*$/.test((p.section.title || ''))) {
                problems.push({
                    file: p.namepath,
                    problem: 'Title is blank.'
                });
            }
            if (/^\s*$/.test((p.section.content || ''))) {
                problems.push({
                    file: p.namepath,
                    problem: 'Content is blank.'
                });
            }
            cb(null, p);
        },
    ], function(err, p) {
        if (!!err) {
            problems.push({
                file: (p || {})
                    .namepath,
                problem: err
            });
            cb(null, null, problems);
        } else {
            p.section.hasProblems = problems.length > 0;
            //give the section back
            logger.debug('[op content] content retrieved.')
            cb(err, p.section, problems);
        }
    });
};
