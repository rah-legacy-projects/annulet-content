var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    util = require('util'),
    line = require('../line');

module.exports = function(options, cb) {
    var problems = [];
    async.waterfall([

        function(cb) {
            //if the name exists, open it
            var namepath = path.resolve(options.path, 'content.md');
            if (!fs.existsSync(namepath)) {
                cb('content missing for op at ' + namepath);
            } else {
                cb(null, {
                    namepath: namepath,
                    section: {
                        __t: 'definitions.operatingProcedure.sectionTypes.SectionedContent',
                        _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.SectionedContent'
                    }
                });
            }

        },
        function(p, cb) {
            //get the name content
            logger.silly('[op sc] gettinc content from ' + p.namepath);
            var content = fs.readFile(p.namepath, function(err, data) {
                p.content = data.toString();
                cb(err, p);
            });
        },
        function(p, cb) {
            //pull the title and desc out of content
            p.section.title = line(p.content, /^\s*#(.+)$/);
            p.section.description = line(p.content, /^\s*([^#].+)$/);
            if (/^\s*$/.test((p.section.title || ''))) {
                problems.push({
                    file: p.namepath,
                    problem: 'Title is blank.'
                });
            }
            //descriptions not mandatory for subsectioned content
            /*
            if (/^\s*$/.test((p.section.description || ''))) {
                problems.push({
                    file: p.namepath,
                    problem: 'Description is blank.'
                });
            }
            */
            cb(null, p);
        },
        function(p, cb) {
            //get the definition files
            fs.readdir(path.resolve(options.path, 'subsections'), function(err, files) {
                async.series(_.map(files, function(file) {
                    return function(cb) {
                        var fpath = path.resolve(options.path, 'subsections', file);
                        fs.stat(fpath, function(err, stat) {
                            if (!stat.isDirectory()) {
                                cb(null, fpath);
                            } else {
                                cb(null);
                            }
                        });
                    };
                }), function(err, r) {
                    p.subfiles = _.filter(r, function(s) {
                        return !!s;
                    });
                    cb(err, p);
                });
            });
        },
        function(p, cb) {
            //get the terms out of the definition files
            async.series(_.map(p.subfiles, function(subfile) {
                return function(cb) {
                    logger.silly('reading ' + subfile);
                    fs.readFile(subfile, function(err, content) {
                        content = content.toString();
                        var sub = {};
                        lines = content.split('\n');
                        sub.title = /^\s*#(.+)$/.exec(lines[0])[1];

                        sub.content = _.chain(lines.slice(1))
                            .dropWhile(function(line) {
                                return /^\s*$/.test(line);
                            })
                            .dropRightWhile(function(line) {
                                return /^\s*$/.test(line);
                            })
                            .value()
                            .join('\n');

                        if (/^\s*$/.test((sub.title || ''))) {
                            problems.push({
                                file: subfile,
                                problem: 'Title is blank.'
                            });
                        }
                        if (/^\s*$/.test((sub.content || ''))) {
                            problems.push({
                                file: subfile,
                                problem: 'Content is blank.'
                            });
                        }
                        cb(err, sub);
                    });
                };
            }), function(err, r) {
                p.section.subSections = r;
                cb(err, p);
            });
        }
    ], function(err, p) {
        if (!!err) {
            problems.push({
                file: (p || {})
                    .namepath,
                problem: err
            });
            cb(null, null, problems);
        } else {
            p.section.hasProblems = problems.length > 0;
            //give the section back
            logger.debug('[op sc] sectioned content retrieved.');
            cb(err, p.section, problems);
        }
    });
};
