var _ = require('lodash'),
    logger = require('winston'),
    async = require('async'),
    path = require('path'),
    fs = require('fs'),
    util = require('util'),
    line = require('../line');

module.exports = function(options, cb) {
    var problems = [];
    async.waterfall([

        function(cb) {
            //if the name exists, open it
            var namepath = path.resolve(options.path, 'content.md');
            if (!fs.existsSync(namepath)) {
                cb('content missing for op at ' + namepath);
            } else {
                cb(null, {
                    namepath: namepath,
                    section: {
                        __t: 'definitions.operatingProcedure.sectionTypes.Definitions',
                        _rangeType: 'definitions.operatingProcedure.ranged.sectionTypes.Definitions'
                    }
                });
            }

        },
        function(p, cb) {
            //get the name content
            var content = fs.readFile(p.namepath, function(err, data) {
                p.content = data.toString();
                cb(err, p);
            });
        },
        function(p, cb) {
            //pull the title and desc out of content
            p.section.title = line(p.content, /^\s*#(.+)$/);
            p.section.description = line(p.content, /^\s*([^#].+)$/);
            if (/^\s*$/.test((p.section.title || ''))) {
                problems.push({
                    file: p.namepath,
                    problem: 'Title is blank.'
                });
            }
            if (/^\s*$/.test((p.section.description || ''))) {
                problems.push({
                    file: p.namepath,
                    problem: 'Description is blank.'
                });
            }
            cb(null, p);
        },
        function(p, cb) {
            logger.silly('[op def] getting definition files');
            //get the definition files
            fs.readdir(path.resolve(options.path, 'terms'), function(err, files) {
                async.series(_.map(files, function(file) {
                    return function(cb) {
                        var fpath = path.resolve(options.path, 'terms', file);
                        logger.silly('[op def] fpath: ' + fpath);
                        fs.stat(fpath, function(err, stat) {
                            if (!!err) {
                                logger.error(err);
                            }
                            if (!stat.isDirectory()) {
                                cb(null, fpath);
                            } else {
                                cb(null);
                            }
                        });
                    };
                }), function(err, r) {
                    p.termfiles = _.filter(r, function(s) {
                        return !!s;
                    });
                    cb(err, p);
                });
            });
        },
        function(p, cb) {
            logger.silly('[op def] getting the terms out of ' + p.termfiles.length + ' files')
            //get the terms out of the definition files
            async.series(_.map(p.termfiles, function(termfile) {
                return function(cb) {
                    fs.readFile(termfile, function(err, content) {
                        var term = {};
                        content = content.toString();
                        lines = content.split('\n');
                        term.term = /^\s*#(.+)$/.exec(lines[0])[1];
                        term.meaning = _.chain(lines.slice(1))
                            .dropWhile(function(line) {
                                return /^\s*$/.test(line);
                            })
                            .dropRightWhile(function(line) {
                                return /^\s*$/.test(line);
                            })
                            .value()
                            .join('\n');

                        if (/^\s*$/.test((term.term || ''))) {
                            problems.push({
                                file: termfile,
                                problem: 'Term is blank.'
                            });
                        }
                        if (/^\s*$/.test((term.meaning || ''))) {
                            return;
                            problems.push({
                                file: termfile,
                                problem: 'Meaning is blank.'
                            });
                        }
                        cb(err, term);
                    });
                };
            }), function(err, r) {
                p.section.terms = r;
                cb(err, p);
            });
        }
    ], function(err, p) {
        p.section.hasProblems = problems.length > 0;
        //give the definition back
        logger.debug('[op def] definition retrieved.');
        cb(err, p.section, problems);
    });
};
