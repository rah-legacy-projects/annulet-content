module.exports = exports = {
	operatingProcedure: require("./operatingProcedure"),
	operatingProcedureSection: require("./operatingProcedureSection"),
	question: require("./question"),
	quiz: require("./quiz"),
	training:require('./training'),
	workflow: require('./workflow'),
	mv: require('./mv')
};
