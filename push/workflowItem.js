var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    path = require('path'),
    login = require('./login'),
    config = require('annulet-config'),
    request = require('request');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(options, cb) {
    //options:
    //{
    //  authToken (optional)
    //  workflow flattened
    //  customer
    //}
    var customerId = _.extractId(options, 'customer');
    async.waterfall([

        function(cb) {
            cb(null, {});
        },
        function(p, cb) {
            //verify workflow
            if (!options.workflow) {
                return cb('No workflow supplied for push.');
            }
            cb(null, p);
        },
        function(p, cb) {
            if (!options.customer) {
                return cb('no customer supplied for push.');
            }
            cb(null, p);
        },
        function(p, cb) {
            //log in if not logged in
            if (!options.authToken) {
                login(null, function(err, authToken) {
                    options.authToken = authToken;
                    cb(err, p);
                });
            } else {
                cb(null, p);
            }
        },
        function(p, cb) {
            //get the workflow by short name
            request.get({
                json: true,
                headers: {
                    'annulet-auth-token': options.authToken,
                    'annulet-auth-customer': customerId
                },
                url: config.configuration.paths.apiUri() + '/admin/workflow/detail/shortName/' + options.workflow.shortName
            }, function(err, response, body) {
                //body.data has the published/draft tuple for the shortname
                logger.silly('tuple retrieved for wf');
                p.tuple = body.data;
                cb(err, p);
            });
        },
        function(p, cb) {
            if (!!p.tuple.draft) {
                //if the tuple's draft exists, use the tuple draft
                p.toUpdate = p.tuple.draft;
                logger.silly('draft used');
                return cb(null, p);
            } else if (!p.tuple.draft && !p.tuple.published) {
                //if neither the draft nor published exists, open a new draft with new
                logger.silly('creating off of new');
                request.post({
                    json: true,
                    headers: {
                        'annulet-auth-token': options.authToken,
                        'annulet-auth-customer': customerId
                    },
                    url: config.configuration.paths.apiUri() + '/admin/workflow/create',
                    body: {
                        _id: 'new'
                    },
                }, function(err, response, body) {
                    logger.silly('wf draft created');
                    p.toUpdate = body.data;
                    cb(err, p);
                })
            } else {
                //otherwise, open a draft on the published version
                logger.silly('creating off of existing');
                request.post({
                    json: true,
                    headers: {
                        'annulet-auth-token': options.authToken,
                        'annulet-auth-customer': customerId
                    },
                    url: config.configuration.paths.apiUri() + '/admin/workflow/create',
                    body: {
                        _id: p.tuple.published._id
                    },
                }, function(err, response, body) {
                    logger.silly('wf draft created');
                    p.toUpdate = body.data;
                    cb(err, p);
                })
            }
        },
        function(p, cb) {
            logger.silly('about to update the wf');
            //clone the options
            var workflowUpdate = _.clone(options.workflow);
            //update options with id/rangeid
            //all pushed changes are treated as new ranges
            workflowUpdate._id = p.toUpdate._id;
            workflowUpdate._rangeId = 'new';
            //update workflow
            request.post({
                json: true,
                headers: {
                    'annulet-auth-token': options.authToken,
                    'annulet-auth-customer': customerId
                },
                url: config.configuration.paths.apiUri() + '/admin/workflow/update',
                body: workflowUpdate
            }, function(err, response, body) {
                logger.silly('wf updated');
                p.update = body.data;
                cb(err, p);
            });
        },
        function(p, cb) {
            //publish the draft
            request.post({
                json: true,
                headers: {
                    'annulet-auth-token': options.authToken,
                    'annulet-auth-customer': customerId
                },
                url: config.configuration.paths.apiUri() + '/admin/workflow/publish',
                body: {
                    _id: p.update._id
                }
            }, function(err, response, data) {
                p.published = data;
                cb(err, p);
            });
        },
    ], function(err, p) {
        cb(err, p.published);
    });
}
