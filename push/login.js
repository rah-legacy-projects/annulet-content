var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    config = require('annulet-config'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options, cb) {
    options = _.defaults(options || {}, {
        email: config.configuration.administrativeUser()
            .email,
        password: config.configuration.administrativeUser()
            .password
    });
    async.waterfall([

        function(cb) {
            cb(null, {});
        },
        function(p, cb) {
            request.post({
                json: true,
                url: config.configuration.paths.authUri() + '/login',
                body: {
                    email: options.email,
                    password: options.password
                }
            }, function(err, response, body) {
                p.authToken = body.data
                cb(err, p);
            });
        },
    ], function(err, p) {
        cb(err, p.authToken);
    });

};
