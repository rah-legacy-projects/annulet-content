var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    config = require('annulet-config'),
    logger = require('winston'),
    fs = require('fs'),
    path = require('path'),
    login = require('./login'),
    request = require('request');

_.mixin(require('annulet-util')
    .lodashMixins);

module.exports = exports = function(options, cb) {
    //options:
    //{
    //  authToken (optional)
    //  quiz flattened
    //  customer
    //}
    var customerId = _.extractId(options, 'customer');
    async.waterfall([

        function(cb) {
            cb(null, {});
        },
        function(p, cb) {
            //verify quiz
            if (!options.quiz) {
                return cb('No quiz supplied for push.');
            }
            cb(null, p);
        },
        function(p, cb) {
            if (!options.customer) {
                return cb('no customer supplied for push.');
            }
            cb(null, p);
        },
        function(p, cb) {
            //log in if not logged in
            if (!options.authToken) {
                login(null, function(err, authToken) {
                    options.authToken = authToken;
                    cb(err, p);
                });
            } else {
                cb(null, p);
            }
        },
        function(p, cb) {
            logger.silly('about to get a quiz by shortname: ' + options.quiz.shortName);
            //get the quiz by short name
            request.get({
                json: true,
                headers: {
                    'annulet-auth-token': options.authToken,
                    'annulet-auth-customer': customerId
                },
                url: config.configuration.paths.apiUri() + '/admin/quiz/detail/shortName/' + options.quiz.shortName
            }, function(err, response, body) {
                //body.data has the published/draft tuple for the shortname
                logger.silly('shortname tuple retrieved');
                p.tuple = body.data;
                cb(err, p);
            });
        },
        function(p, cb) {
            if (!!p.tuple.draft) {
                //if the tuple's draft exists, use the tuple draft
                p.toUpdate = p.tuple.draft;
                return cb(null, p);
            } else if (!p.tuple.draft && !p.tuple.published) {
                //if neither the draft nor published exists, open a new draft with new
                request.post({
                    json: true,
                    headers: {
                        'annulet-auth-token': options.authToken,
                        'annulet-auth-customer': customerId
                    },
                    url: config.configuration.paths.apiUri() + '/admin/quiz/create',
                    body: {
                        _id: 'new'
                    },
                }, function(err, response, body) {
                    p.toUpdate = body.data;
                    cb(err, p);
                })
            } else {
                //otherwise, open a draft on the published version
                request.post({
                    json: true,
                    headers: {
                        'annulet-auth-token': options.authToken,
                        'annulet-auth-customer': customerId
                    },
                    url: config.configuration.paths.apiUri() + '/admin/quiz/create',
                    body: {
                        _id: p.tuple.published._id
                    },
                }, function(err, response, body) {
                    p.toUpdate = body.data;
                    cb(err, p);
                })
            }
        },
        function(p, cb) {
            //clone the options
            var quizUpdate = _.clone(options.quiz);
            //update options with id/rangeid
            //all pushed changes are treated as new ranges
            quizUpdate._id = p.toUpdate._id;
            //hack: use the only range of the draft id
            quizUpdate._rangeId = p.toUpdate.rangedData[0]._id;
            //update quiz
            request.post({
                json: true,
                headers: {
                    'annulet-auth-token': options.authToken,
                    'annulet-auth-customer': customerId
                },
                url: config.configuration.paths.apiUri() + '/admin/quiz/update',
                body: quizUpdate
            }, function(err, response, body) {
                logger.silly('quiz updated');
                p.update = body.data;
                cb(err, p);
            });
        },
        function(p, cb) {
            //publish the draft
            request.post({
                json: true,
                headers: {
                    'annulet-auth-token': options.authToken,
                    'annulet-auth-customer': customerId
                },
                url: config.configuration.paths.apiUri() + '/admin/quiz/publish',
                body: {
                    _id: p.update._id
                }
            }, function(err, response, data) {
                p.published = data.data;
                cb(err, p);
            });
        },
    ], function(err, p) {
        cb(err, p.published);
    });
}
