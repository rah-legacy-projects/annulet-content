module.exports = exports = {
	listCustomers: require("./listCustomers"),
	login: require("./login"),
	operatingProcedure: require("./operatingProcedure"),
	quiz: require("./quiz"),
	training: require("./training"),
	workflowItem: require("./workflowItem"),
};
