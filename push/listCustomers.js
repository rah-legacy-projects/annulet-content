var _ = require('lodash'),
    async = require('async'),
    modeler = require('annulet-models')
    .modeler,
    projectUtility = require('annulet-models')
    .util,
    util = require('util'),
    logger = require('winston'),
    fs = require('fs'),
    login = require('./login'),
    request = require('request'),
    path = require('path');

_.mixin(require('annulet-util')
    .lodashMixins);
module.exports = exports = function(options, cb) {
    async.waterfall([

        function(cb) {
            cb(null, {});
        },
        function(p, cb) {
            //log in if not logged in
            if (!options.authToken) {
                login(null, function(err, authToken) {
                    options.authToken = authToken;
                    cb(err, p);
                });
            } else {
                cb(null, p);
            }
        },
        function(p, cb){
            request({
                json:true,
                url: config.configuration.paths.apiUri() + '/admin/customer',
                headers:{
                    'annulet-auth-token': p.authToken
                }
            },function(err, response, body){
                p.customers = body.data;
                cb(err, p);
            });
        }
    ], function(err, p) {
        cb(err, p.customers);
    });

};
